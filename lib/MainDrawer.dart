import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(color: Theme.of(context).primaryColor),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Jack Zomboe",
                    style: TextStyle(fontSize: 20),
                  ),
                  DropdownButton<String>(
                    value: "aufa.nabil.amiri@gmail.com",
                    underline: Container(
                      height: 0,
                    ),
                    onChanged: (val) {},
                    items: <String>[
                      'aufa.nabil.amiri@gmail.com',
                      "Profile",
                      'Logout',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  )
                ]),
          ),
          ListTile(title: Text("Dashboard")),
          ListTile(title: Text("List Account")),
          ListTile(title: Text("List Activity")),
        ],
      ),
    );
  }
}
