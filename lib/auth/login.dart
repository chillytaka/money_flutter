import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey =
      GlobalKey<FormState>(); //save form key to control the form widget

  void _onLoginPressed() {
    final form = _formKey.currentState;

    form.validate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Login",
                style: Theme.of(context).textTheme.title,
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                style: Theme.of(context).textTheme.body1,
                decoration: InputDecoration(
                  icon: IconTheme(
                    child: Icon(Icons.email),
                    data: Theme.of(context).iconTheme,
                  ),
                  labelText: "Email",
                ),
                validator: (String value) {
                  return value.contains('@')
                      ? null
                      : 'not a valid email address';
                },
              ),
              TextFormField(
                style: Theme.of(context).textTheme.body1,
                obscureText: true,
                decoration: InputDecoration(
                  icon: IconTheme(
                    child: Icon(Icons.lock),
                    data: Theme.of(context).iconTheme,
                  ),
                  labelText: "Password",
                ),
                validator: (String value) {
                  return value.length >= 8
                      ? null
                      : "password length must be greater than 8";
                },
              ),
              SizedBox(height: 50),
              SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  child: Text("Login"),
                  onPressed: _onLoginPressed,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
