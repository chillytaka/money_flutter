import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKey =
      GlobalKey<FormState>(); //save form key to control the form widget

  final textPassController =
      new TextEditingController(); //get typed password on register

  void _onRegisterPressed() {
    final form = _formKey.currentState;

    form.validate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Register",
                style: Theme.of(context).textTheme.title,
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                style: Theme.of(context).textTheme.body1,
                decoration: InputDecoration(
                  icon: IconTheme(
                    child: Icon(Icons.person),
                    data: Theme.of(context).iconTheme,
                  ),
                  labelText: "Username",
                ),
                validator: (String value) {
                  return value.isEmpty ? "this field cant be empty" : null;
                },
              ),
              TextFormField(
                style: Theme.of(context).textTheme.body1,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  icon: IconTheme(
                    child: Icon(Icons.email),
                    data: Theme.of(context).iconTheme,
                  ),
                  labelText: "Email",
                ),
                validator: (String value) {
                  return value.contains('@')
                      ? null
                      : 'not a valid email address';
                },
              ),
              TextFormField(
                style: Theme.of(context).textTheme.body1,
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly
                ],
                decoration: InputDecoration(
                  icon: IconTheme(
                    child: Icon(Icons.phone),
                    data: Theme.of(context).iconTheme,
                  ),
                  labelText: "Phone Number",
                ),
                validator: (String value) {
                  return value.length <= 10
                      ? "phone number must be greather than 10 digits"
                      : null;
                },
              ),
              TextFormField(
                style: Theme.of(context).textTheme.body1,
                controller: textPassController,
                obscureText: true,
                decoration: InputDecoration(
                  icon: IconTheme(
                    child: Icon(Icons.lock),
                    data: Theme.of(context).iconTheme,
                  ),
                  labelText: "Password",
                ),
                validator: (String value) {
                  return value.length >= 8
                      ? null
                      : "password length must be greater than 8";
                },
              ),
              TextFormField(
                style: Theme.of(context).textTheme.body1,
                obscureText: true,
                decoration: InputDecoration(
                  icon: IconTheme(
                    child: Icon(Icons.lock),
                    data: Theme.of(context).iconTheme,
                  ),
                  labelText: "Confirm Password",
                ),
                validator: (String value) {
                  return value.isEmpty
                      ? "this field cant be empty"
                      : value != textPassController.text
                          ? "password mismatch"
                          : null;
                },
              ),
              SizedBox(height: 50),
              SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  child: Text("Register"),
                  onPressed: _onRegisterPressed,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
