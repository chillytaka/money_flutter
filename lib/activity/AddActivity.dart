import 'package:flutter/material.dart';
import 'package:money_flutter/MainDrawer.dart';

class AddActivity extends StatelessWidget {
  final _listAccount = <String>["Acc #1", "Acc #2", "Acc #3"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text("Add Activity"),
        actions: <Widget>[
          MaterialButton(
            onPressed: null,
            child: Text(
              "ADD",
              style: Theme.of(context).textTheme.button,
            ),
          )
        ],
      ),
      body: SafeArea(
          child: Container(
        padding: EdgeInsets.all(16),
        child: ListView(children: <Widget>[
          //Account Number
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Account No."),
              DropdownButton<String>(
                value: _listAccount[0],
                onChanged: (val) {},
                items:
                    _listAccount.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ],
          ),
          Divider(
            thickness: 1,
            color: Theme.of(context)
                .inputDecorationTheme
                .enabledBorder
                .borderSide
                .color,
          ),
          SizedBox(height: 10),
          //End Of Account Number
          TextField(
            maxLines: 8,
            decoration: InputDecoration(
                hintText: "Activity Details",
                hintStyle: TextStyle(color: Colors.white38),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white38)),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white))),
          ),
          TextField(
            decoration: InputDecoration(labelText: "Activity Ammount"),
          )
        ]),
      )),
    );
  }
}
