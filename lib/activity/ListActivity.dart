import 'package:flutter/material.dart';
import 'package:money_flutter/MainDrawer.dart';
import 'package:money_flutter/reusable/TappableListTile.dart';

class ListActivity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text("List Activity"),
      ),
      body: ListView(
        children: <Widget>[
          TappableListTile(
            title: "Monthly Payment",
            subtitle: "Acc #1",
            trailingText: "+ Rp. 15000",
          ),
          TappableListTile(
            title: "Monthly Payment",
            subtitle: "Acc #1",
            trailingText: "- Rp. 15000",
          ),
          TappableListTile(
            title: "Monthly Payment",
            subtitle: "Acc #1",
            trailingText: "+ Rp. 150000",
          )
        ],
      ),
    );
  }
}
