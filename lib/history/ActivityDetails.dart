import 'package:flutter/material.dart';
import 'package:money_flutter/MainDrawer.dart';
import 'package:money_flutter/reusable/DetailsText.dart';

class ActivityDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text("Activity Details"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            DetailsText(title: "Activity ID", content: "#120"),
            DetailsText(title: "Activity Type", content: "Withdrawal"),
            DetailsText(title: "Activity Amount", content: "Rp. 120.000")
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.delete),
        onPressed: () => {},
      ),
    );
  }
}
