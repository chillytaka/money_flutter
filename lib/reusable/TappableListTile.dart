import 'package:flutter/material.dart';

class TappableListTile extends StatelessWidget {
  final String title;
  final String subtitle;
  final IconData icon;
  final String trailingText;

  TappableListTile(
      {Key key,
      @required this.title,
      this.subtitle,
      this.icon,
      this.trailingText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(title),
        subtitle: Text(subtitle),
        trailing: icon != null
            ? Icon(
                icon,
                color: Theme.of(context).iconTheme.color,
              )
            : Text(trailingText != null ? trailingText : "N/A"));
  }
}
