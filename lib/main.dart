import 'package:flutter/material.dart';
import 'package:money_flutter/account/FormAccount.dart';
import 'package:money_flutter/account/ListAccount.dart';
import 'package:money_flutter/activity/AddActivity.dart';
import 'package:money_flutter/activity/ListActivity.dart';
import 'package:money_flutter/history/AccountHistory.dart';
import 'package:money_flutter/auth/login.dart';
import 'package:money_flutter/auth/register.dart';
import 'package:money_flutter/dashboard.dart';
import 'package:money_flutter/history/ActivityDetails.dart';
import 'package:money_flutter/profile/MainProfile.dart';
import 'package:money_flutter/splash.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MoMon',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        scaffoldBackgroundColor: Colors.black87,
        backgroundColor: Colors.grey[700],
        primaryColor: Colors.blue,
        accentColor: Colors.blueAccent,
        canvasColor: Colors.grey[800],
        iconTheme: IconThemeData(color: Colors.white),
        cardTheme: CardTheme(color: Colors.grey[800]),
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.lightBlue,
          textTheme: ButtonTextTheme.normal,
        ),
        inputDecorationTheme: InputDecorationTheme(
          labelStyle: TextStyle(color: Colors.white38),
          hintStyle: TextStyle(color: Colors.white38),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white54),
          ),
        ),
        textTheme: TextTheme(
          subtitle1: TextStyle(color: Colors.white),
          subtitle2: TextStyle(color: Colors.white),
          caption: TextStyle(color: Colors.white54),
          headline6: TextStyle(
            color: Colors.white,
            fontSize: 40.0,
          ),
          bodyText1: TextStyle(color: Colors.white),
          bodyText2: TextStyle(color: Colors.black),
          button: TextStyle(color: Colors.white),
        ),
      ),
      home: MainProfile(),
    );
  }
}
