import 'package:flutter/material.dart';

class EmailCard extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(hintText: "Email"),
                validator: (String value) {
                  return value.contains('a')
                      ? null
                      : 'not a valid email address';
                },
              ),
              SizedBox(height: 16),
              Container(
                width: double.infinity,
                child: RaisedButton(
                  onPressed: () {},
                  child: Text("Change Email"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
