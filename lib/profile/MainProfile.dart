import 'package:flutter/material.dart';
import 'package:money_flutter/profile/DeleteCard.dart';
import 'package:money_flutter/profile/EmailCard.dart';
import 'package:money_flutter/profile/PasswordCard.dart';
import 'package:money_flutter/profile/UsernameCard.dart';

class MainProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: ListView(
            children: <Widget>[
              UsernameCard(),
              EmailCard(),
              PasswordCard(),
              DeleteCard()
            ],
          ),
        ),
      ),
    );
  }
}
