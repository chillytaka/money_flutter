import 'package:flutter/material.dart';

class UsernameCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            TextField(
              decoration: InputDecoration(hintText: "Username"),
            ),
            SizedBox(height: 16),
            Container(
              width: double.infinity,
              child: RaisedButton(
                onPressed: () {},
                child: Text("Change Username"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
