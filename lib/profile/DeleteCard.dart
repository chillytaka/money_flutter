import 'package:flutter/material.dart';

class DeleteCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(16),
        child: Container(
          width: double.infinity,
          child: RaisedButton(
            color: Colors.redAccent,
            onPressed: () {},
            child: Text("Delete Account"),
          ),
        ),
      ),
    );
  }
}
