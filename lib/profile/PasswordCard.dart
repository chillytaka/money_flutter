import 'package:flutter/material.dart';

class PasswordCard extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final textPassController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                controller: textPassController,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: "New Password",
                ),
                validator: (String value) {
                  return value.length >= 8
                      ? null
                      : "password length must be greater than 8";
                },
              ),
              TextFormField(
                obscureText: true,
                decoration: InputDecoration(
                  hintText: "Confirm New Password",
                ),
                validator: (String value) {
                  return value.isEmpty
                      ? "this field cant be empty"
                      : value != textPassController.text
                          ? "password mismatch"
                          : null;
                },
              ),
              TextFormField(
                obscureText: true,
                decoration: InputDecoration(
                  hintText: "Old Password",
                ),
                validator: (String value) {
                  return value.isEmpty ? "this field cant be empty" : null;
                },
              ),
              SizedBox(height: 16),
              Container(
                width: double.infinity,
                child: RaisedButton(
                  onPressed: () {},
                  child: Text("Change Password"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
