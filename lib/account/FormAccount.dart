import 'package:flutter/material.dart';
import 'package:money_flutter/MainDrawer.dart';

class FormAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text("Add Account"),
        actions: <Widget>[
          MaterialButton(
            onPressed: null,
            child: Text(
              "ADD/SAVE",
              style: Theme.of(context).textTheme.button,
            ),
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            TextField(
              decoration: InputDecoration(labelText: "Account Name"),
            ),
            SizedBox(height: 10),
            TextField(
              maxLines: 8,
              decoration: InputDecoration(
                  hintText: "Account Details",
                  hintStyle: TextStyle(color: Colors.white38),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white38)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white))),
            ),
            SizedBox(height: 10),
            TextField(
              decoration: InputDecoration(labelText: "Account Ammount"),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.delete),
      ),
    );
  }
}
