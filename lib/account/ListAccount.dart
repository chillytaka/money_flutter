import 'package:flutter/material.dart';
import 'package:money_flutter/MainDrawer.dart';
import 'package:money_flutter/reusable/TappableListTile.dart';

class ListAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text("List Account"),
      ),
      body: SafeArea(
        child: Container(
          child: ListView(
            children: <Widget>[
              TappableListTile(
                title: "Acc #1",
                icon: Icons.edit,
                subtitle: "Rp. 50,000",
              ),
              TappableListTile(
                title: "Acc #2",
                icon: Icons.edit,
                subtitle: "Rp. 150,000",
              ),
              TappableListTile(
                title: "Acc #3",
                icon: Icons.edit,
                subtitle: "Rp. 20,000",
              ),
              TappableListTile(
                title: "Acc #4",
                icon: Icons.edit,
                subtitle: "Rp. 30,000",
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: null,
        child: Icon(Icons.add),
      ),
    );
  }
}
