import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:money_flutter/MainDrawer.dart';
import 'package:money_flutter/dummy.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverSafeArea(
            sliver: SliverAppBar(
              title: Text("Dashboard"),
              pinned: true,
              expandedHeight: 200.0,
              flexibleSpace: FlexibleSpaceBar(
                background: Container(
                  padding: EdgeInsets.only(bottom: 40),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "Total Balance",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.w500),
                      ),
                      Text(
                        "Rp. 150.000",
                        style: TextStyle(
                            fontSize: 50.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "this value is shared across all accounts",
                        style: TextStyle(color: Colors.white60),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Dummy(),
        ],
      ),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        overlayColor: Colors.black,
        children: [
          SpeedDialChild(
            child: Icon(Icons.account_box),
            label: "Add Account",
            labelBackgroundColor: Theme.of(context).backgroundColor,
          ),
          SpeedDialChild(
            child: Icon(Icons.timelapse),
            label: "Add Activity",
            labelBackgroundColor: Theme.of(context).backgroundColor,
          ),
        ],
      ),
    );
  }
}
